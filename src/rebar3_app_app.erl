%%%-------------------------------------------------------------------
%% @doc rebar3_app public API
%% @end
%%%-------------------------------------------------------------------

-module(rebar3_app_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
  Namea = rebar3_module_a:show_name(),
  Nameb = rebar3_module_b:show_name(),
%%  cowboy:start(),
  io:fwrite("Names ~p~n", [Namea]),
  io:fwrite("Names ~p~n", [Nameb]),
  rebar3_app_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
  ok.

%%====================================================================
%% Internal functions
%%====================================================================
